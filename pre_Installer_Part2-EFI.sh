#!/bin/bash
################################################
## Pre installer script                       ##
## Script to automagically install Arch Linux ##
## YMMV: Use at your own Risk                 ##
################################################
# List of variables
HD=/dev/sda
#HDUUID=$(lsblk -f | grep $HD | awk '{print $3}')
HDUUID=$(blkid $HD | awk '{print $2}' | grep -Po '".*?"' | cut -d "\"" -f 2)
bootVol=/dev/sda1 # volume  to put /boot stuffs
bootVolUUID=$(lsblk -f | grep $bootVol | awk '{print $3}')
cryptVol=/dev/sda2 # encrypt the / voluem and run LVM in that.
#cryptVol=/dev/mapper/VolGroup00-ROOT_Vol # encrypt the / voluem and run LVM in that.
cryptVolUUID=$(lsblk -f | grep $cryptVol | awk '{print $3}')
FS=btrfs  # could also be: xfs,zfs,ext4,etc...
rootPATH=$(blkid /dev/VolGroup00/ROOT_Vol -sUUID -ovalue)
resumePATH=$(blkid /dev/VolGroup00/SWAP_Vol -sUUID -ovalue)
export TZ=/usr/share/zoneinfo/America/New_York # set Timezone to America/New_York
# Start of script.
##########################################################
# To be run after you are CHROOTED into your environment #
##########################################################
# set the time zone.
ln -sf $TZ /etc/localtime
# run hwclock(8) to generate /etc/adjtime:
hwclock --systohc
# set the locale
echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" >> /etc/locale.conf
# skipping keymap, beacue im english US...
# If you set the keyboard layout, make the changes persistent in vconsole.conf(5):
#/etc/vconsole.conf
#KEYMAP=de-latin1
# Set your systems hostname
echo "Set your systems hostname"
read sethostname
echo $sethostname > /etc/hostname
# setup /etc/hosts file
echo "127.0.0.1	localhost" > /etc/hosts
echo "::1		localhost" >> /etc/hosts
echo "127.0.1.1	"$sethostname".localdomain	"$sethostname >> /etc/hosts
#
# Set root password:
#
echo "Set root password"
passwd root
# setup wheel group in /etc/sudoers
echo "%wheel ALL=(ALL) ALL" >> /etc/sudoers
# turn on networking
systemctl enable --now NetworkManager
# Grab a copy of the installers for post install reasons.
cd
git clone git@gitlab.com:phreekbird_public/ArchInstaller.git
# enable multilib
echo "[multilib]" >> /etc/pacman.conf
echo "Include = /etc/pacman.d/mirrorlist" >> /etc/pacman.conf
#
# setup systemd-boot
bootctl --path=/boot install
# setup boot loader
echo "title    Arch Linux
linux    /vmlinuz-linux
initrd    /intel-ucode.img
initrd    /initramfs-linux.img
options    cryptdevice="$cryptVolUUID":cryptlvm root=UUID="$rootPATH" resume=UUID="$resumePATH"" >> /boot/loader/entries/arch.conf

#cp our own mkinitcpio file, with fixes for cryptdevices
mv /etc/mkinitcpio.conf /etc/mkinitcpio.conf.ORIG.BAK
cp ~/ArchInstaller-master/mkinitcpio.conf /etc/
mkinitcpio -P
echo "All done, reboot your system now. once done, run one of the post install scripts."

# ArchInstaller
Dirty little script to do a base OS arch install, with post install scripts ... because I am lazy and refuse to remember all the commands, that's why.

These scripts assume a few things.
1. your HD is /dev/sda
2. /dev/sda1 is your /boot
3. /dev/sda2 is an encrypted volume.
4. /dec/sda2 once decrypted, will have LVM volumes (SWAP_Vol and ROOT_Vol)
5. that you already know how FDISK works... as the script calls it. Its up to you to figure it out. once you write the partition scheme the script keeps going, regardless if you got your partitioning correct or not... like i said, dirty scripts.
6. that you are using a gpt partitioned disk with uEFI. if your using MBR, then make sure to run the Part2-MBR script, otherwise run the Part2-EFI script.

Some of these things can be changed in the scripts (at the top of each script, I tried to put the variables).

NOTE: edit the variables in both preinstall scripts. for example, if you know your /dev/sda1 is /boot and your /dev/sda2 is an encrypted volume, fix the variables in the pre install scripts. Also look at the grub file, specifically GRUB_CMDLINE_LINUX="cryptdevice=/dev/sda2:cda" and fix whichever device is your encrypted volume. I am lazy and i just do a mv of the original files (to filename.ORIG.BAK) and then copy in my own in to replace them.

# Pre install steps
1. Edit variables in both pre_Install scripts.
2. Edit grub file.

# Base OS install (Part One)
1. Launch new system with the Arch Live ISO:: you can get it from here: https://www.archlinux.org/download/
2. setup networking, if you havnt already (wifi-menu for wireless).
3. run a quick wget https://goo.gl/HvxkMP <-new gitlab link, =P Micro$haft and github.
4. pacman -Syy & pacman -S pacman-contrib unzip --noconfirm
5. unzip HvxkMP
6. cd ArchInstaller-master
7. chmod a+x *.sh
8. setup your base system with: ./pre_Installer_Part1.sh

# Base OS install (Part Two)
Now that your chrooted into your newly installed environment, its time to finish the base OS install, and setup the bootloader.
1. cd ~/ArchInstaller-master
2. ./pre_Installer_Part2-EFI.sh OR ./pre_Installer_Part2-MBR.sh ... depending on if you do an MBR or GPT/EFI install.
Please note that the MBR version is no longer maintained and is outdated... YMMV.

## NOTE
There is an error with listing the block devices and pulling the UUID of /dev/sda and inserting it into the arch.conf file.
Run the following manually:

```blkid $HD | awk '{print $2}' | grep -Po '".*?"' | cut -d "\"" -f 2 >> /boot/loader/entries/arch.conf```

This will output the hard disk (/dev/sda) UUID to a new line in the arch.conf file.
now VIM that arch.conf file ```vim /boot/loader/entries/arch.conf```
and copy the UUID and put it into the cryptdevice=UUID="" spot. (you can use 'yy', and 'p', to yank/put respectivly) 
or use any editor of your choice.
You may then delete the new line UUID at the bottom of the file. (in VI just use 'dd' to remove the line)

3. reboot

# Make it pretty (Part Three)
Now that you rebooted, you should have a very rough OS. no graphics, just CLI and only root account...
1. AS ROOT ---> Make sure to setup wifi: wifi-menu
2. Add a new user account: useradd -m USERNAME and also usermod -aG wheel USERNAME so that you have sudo access. and lastly set the password for the user with passwd USERNAME
3. As root log into your system as the new user: su - USERNAME
4. run a quick wget https://goo.gl/HvxkMP
5. unzip HvxkMP
6. cd ArchInstaller-master
7. chmod a+x *.sh
8. Now pick your poison, either INTEL, ATI, or NVIDIA... and run one of those post install scripts.
There are 3 Post Install scripts, one for INTEL, NVIDIA, and ATI GPU's. Not sure if they all work as they are not really tested. Each script will install a ton of packages, including Gnome3 Desktop. If you dont like it, change it, and make it your own.

Also note the post install scripts create a file /usr/bin/updateATT ... it literally updates all the things... and screw security, i tell it to noconfirm and force... again if you dont like, change it and make it your own.

Your mileage/km will likely vary, so use at your own risk.

Feel your Taters!

~Phreek

#!/bin/bash
################################################
## Post installer script                      ##
## Script to automagically install Arch Linux ##
## YMMV: Use at your own Risk                 ##
################################################
# setup the updateATT script
sudo cat <<EOF > /usr/bin/updateATT
#!/bin/bash
# this script will auto update any gpg keys, and software using Pacman AND Yay.
# use at your own risk!
sudo pacman -S archlinux-keyring --force --noconfirm && sudo pacman-key --refresh-keys && sudo pacman -Syuq --force --noconfirm && yay -Syuq --force --noconfirm
EOF
chmod a+x /usr/bin/updateATT
# setup an auto update script for pacman
# For info and troubleshooting see the following:
# https://www.techrapid.uk/2017/04/automatically-update-arch-linux-with-systemd.html

# Setup the autoupdate.service file
sudo cat <<EOF > /etc/systemd/system/autoupdate.service
[Unit]
 Description=Automatic Update
 After=network-online.target 

[Service]
 Type=simple
 ExecStart=/usr/bin/pacman -Syuq --noconfirm
 TimeoutStopSec=180
 KillMode=process
 KillSignal=SIGINT

[Install]
 WantedBy=multi-user.target
EOF
# Setup the autoupdate.timer file
cat <<EOF > /etc/systemd/system/autoupdate.timer
[Unit]
 Description=Automatic Update when booted up after 5 minutes then check the system for updates every 60 minutes

[Timer]
 OnBootSec=5min
 OnUnitActiveSec=60min
 Unit=autoupdate.service

[Install]
 WantedBy=multi-user.target
EOF
# Enable the autoupdate.timer to run at startup
sudo systemctl enable /etc/systemd/system/autoupdate.timer
# setup ntp
sudo systemctl start ntpd.service
sudo systemctl enable ntpd.service
# Do a quick refresh of all installed packages
sudo pacman -Syy --noconfirm --force
# Lets install everything!
sudo pacman -S --noconfirm --force --needed xf86-video-intel lib32-mesa-libgl openssh python flashplugin parted gparted gptfdisk ntfs-3g dosfstools cups hplip wine wine-mono wine_gecko lib32-ncurses lib32-mpg123 lib32-alsa-plugins lib32-libpulse lib32-openal samba gnome gnome-extra gnome-packagekit polkit-gnome gzip sip p7zip gst-libav hunspell-en telepathy alacarte seahorse-nautilus system-config-printer glade freerdp openssh nmap vinagre remmina gvfs-afc gvfs-smb gvfs-gphoto2 gvfs-mtp gvfs-goa gimp firefox libreoffice-fresh mypaint libdvdcss atom gedit brasero calibre cheese deluge baobab gnome-games librecad nautilus nautilus-sendto nm-connection-editor gnome-nettool virtualbox system-config-printer seahorse gnome-terminal fish gnome-weather winetricks gdm gnome-initial-setup gnome-tweak-tool chrome-gnome-shell
# setup a user to do yay installer
useradd -m archinstaller
usermod -aG wheel archinstaller
# lets get yay installed
cd /home/archinstaller/
runuser -l  archinstaller -c 'git clone https://aur.archlinux.org/yay.git'
runuser -l  archinstaller -c 'cd yay'
runuser -l  archinstaller -c 'makepkg -si'
# add chrome and teamviewer pacaur, and oracle java 8
runuser -l  archinstaller -c 'yay -S jdk8 pamac-aur google-chrome teamviewer --noconfirm --force'
# remove the archinstaller user account
sudo userdel archinstaller
# last but not least, lets enable some things at boot
sudo systemctl enable gdm.service
sudo systemctl enable NetworkManager.service
sudo systemctl enable avahi-daemon.service
sudo systemctl enable org.cups.cupsd
sudo systemctl enable org.cups.cupsd.service
# setup user
echo "Add a User:"
read username
sudo useradd -m $username
sudo gpasswd -a $username video
sudo gpasswd -a $username wheel
echo "Set" $username "password:"
sudo passwd $username
echo "User " $username " has been created."
# Thats it reboot.
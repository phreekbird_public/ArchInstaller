#!/bin/bash
################################################
## Pre installer script                       ##
## Script to automagically install Arch Linux ##
## YMMV: Use at your own Risk                 ##
################################################
# List of variables
HD=/dev/sda
bootVol=/dev/sda1 # volume  to put /boot stuffs
cryptVol=/dev/sda2 # encrypt the / voluem and run LVM in that.
FS=btrfs  # could also be: xfs,zfs,ext4,etc...
export TZ=/usr/share/zoneinfo/America/New_York # set Timezone to America/New_York
# Start of script.
echo "You MUST have networking setup before you run this script!"
echo "Attempting to run wireless setup. If wired, ignore this, and proceed to testing."
read -p "Press [ENTER] key to continue..."
wifi-menu
echo "Testing Networking by pinging archlinux.org"
read -p "Press [ENTER] key to continue..."
# Connect to the Internet
# The installation image enables the dhcpcd daemon on boot for wired network devices. The connection may be checked with:
ping -c 4 archlinux.org
# https://wiki.archlinux.org/index.php/Installation_guide#Connect_to_the_Internet
# https://wiki.archlinux.org/index.php/Wireless_network_configuration
#
# Setup Mirros, this is going to take some time, so we will do it in the BG
echo "Setting up mirrors and ranking them in the background."
# first lets get a rankmirrors app installed
# update current mirrors
pacman -Syy
# and grab rankmirrors command from the pacman-contrib package.
pacman -S pacman-contrib unzip --noconfirm
# grab an updated mirrorlists
# pacman -S pacman-mirrorlists --noconfirm
# backup orig mirrorlist
cp /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.ORIG.backup
# these mirrors are for US only.
cp rankmirrors.list /etc/pacman.d/mirrorlist.backup
# Uncomment all the mirrors
sed -i 's/^#Server/Server/' /etc/pacman.d/mirrorlist.backup
# rank the mirrors, only pull the top 10
rankmirrors -n 10 /etc/pacman.d/mirrorlist.backup > /etc/pacman.d/mirrorlist &
# Verify the boot mode
# If UEFI mode is enabled on an UEFI motherboard, Archiso will boot Arch Linux accordingly via systemd-boot. To verify this, list the efivars directory:
echo "Checking if system is EFI enabled."
echo "If UEFI mode is enabled on an UEFI motherboard, Archiso will boot Arch Linux accordingly via systemd-boot. To verify this, list the efivars directory:"
echo "Listing the /sys/firmware/efi/efivars directory. If nothing exist, setup computer in MBR mode instead."
read -p "Press [ENTER] key to continue..."
ls /sys/firmware/efi/efivars
echo "setting up NTP"
# If no connection is available, stop the dhcpcd service with systemctl stop dhcpcd@ and pressing Tab. Proceed with Network configuration for wired devices or Wireless network configuration for wireless devices.
# Update the system clock
# Use timedatectl(1) to ensure the system clock is accurate:
timedatectl set-ntp true
#
# You will need to setup your own HD.
#
# This assumes HD volume is /dev/sda, you may need to change it accordingly!
echo "Setup your Hard disk as you see fit."
echo "Example /boot = /dev/sda1 and an encrypted LVM volume on /dev/sda2."
echo "NOTE: 8e00 == Linux LVM Type and ef00 == EFI System Type"
echo "dont forget to set the HD types!!!"
read -p "Press [ENTER] key to continue..."
cgdisk $HD
# Setup btrfs for /boot
# mkfs.btrfs -f -L bootVol $bootVol
mkfs.fat -F32 -n BOOTVOL /dev/sda1
# Setup disk encryption
echo "Setting /dev/sda2 (or whatever device you want) as am encrypted partition."
cryptsetup -v --cipher aes-xts-plain64 --key-size 512 --hash sha256 --iter-time 5000 --use-urandom --verify-passphrase luksFormat $cryptVol
# lets unlock the disk and setup LVM inside it.
cryptsetup luksOpen $cryptVol cda
# lets create an LVM
lvmdiskscan
#Physical volume
pvcreate /dev/mapper/cda
pvdisplay
# Volume Group
vgcreate VolGroup00 /dev/mapper/cda
vgdisplay
# Logical Volume
echo "How big do you want your swap space?"
echo "General rule of thumb is 2xRAM up to 32Gigs of SWAP Space, thats usually overkill unless its a Server."
echo "That said, I usually keep my swap space at around 8GB, while my ram is roughly 16GB."
echo "For systems with less RAM, I would opt for a bit more SWAP space."
echo "Swap size in GB ex: 8G"
read swapSize
lvcreate -L $swapSize  VolGroup00 -n SWAP_Vol /dev/mapper/cda
lvcreate -l 100%FREE  VolGroup00 -n ROOT_Vol /dev/mapper/cda
# Make the filesystems
mkfs.btrfs -L rootVol /dev/VolGroup00/ROOT_Vol
mkswap /dev/VolGroup00/SWAP_Vol
# turn on SWAP
swapon /dev/VolGroup00/SWAP_Vol
# Mount the root volume
mount -o compress=lzo /dev/VolGroup00/ROOT_Vol /mnt
# setup /boot and /boot/EFI
mkdir /mnt/boot
mkdir /mnt/boot/EFI
# lets do a link to /boot/efi too.
ln -s /mnt/boot/EFI /mnt/boot/efi
# mount the /boot volume
mount /dev/sda1 /mnt/boot
#
# Install the base packages
#
echo "Installing the base system with pactrap."
echo "Go get some coffee, this is going to take a few..."
read -p "We are about to pacstrap this beast, pres [ENTER] key to continue..."
pacman -Syy
pacstrap /mnt base base-devel git wget yajl dialog netctl iw wpa_supplicant networkmanager btrfs-progs lvm2 archlinux-keyring intel-ucode efibootmgr ntp curl zip gzip unzip vim htop gtop fish parted gptfdisk ntfs-3g dosfstools --noconfirm --force
#
# Setup the system
#
# Generate FSTAB
genfstab -U /mnt >> /mnt/etc/fstab
# before we chroot, lets copy everything over to our new environemnt.
cp -R ~/ArchInstaller-master /mnt/root/
# chroot into the environment
arch-chroot /mnt
echo "All done, no go run part two, now that your chrooted."
